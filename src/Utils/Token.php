<?php

namespace R2Soft\Security\Utils;

class Token
{
    public const PARAM_AUTH_HEADER = "Authorization";

    public const PARAM_PREFIX_TOKEN = "Bearer ";

    public static function getValueOfPayload($key, $token)
    {
        $payload = explode(".", $token)[1];
        $payload = json_decode(base64_decode($payload, true), true);
        return $payload[$key] ?? null;
    }

    public static function getTokenWithoutPrefix($token)
    {
        return str_replace(self::PARAM_PREFIX_TOKEN, "", $token);
    }

    public static function getTokenPrefix($token)
    {
        return self::PARAM_PREFIX_TOKEN . $token;
    }
}
