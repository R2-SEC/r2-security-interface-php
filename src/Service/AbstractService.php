<?php
/*
 * AbstractService.php
 * Copyright (c) R2 Soft Informatica e Softwares Ltda.
 *
 * Este software é confidencial e propriedade da R2 Soft Informatica e Softwares Ltda.
 * Não é permitida sua distribuição ou divulgação do seu conteúdo sem expressa autorização da R2 Soft Informatica e Softwares Ltda.
 * Este arquivo contém informações proprietárias.
 */
namespace R2Soft\Security;

use R2Soft\Security\Utils\HttpResponseCode;

/**
 * Classe abstrata de serviço.
 *
 * @author Paulo Leonardo de O. Miranda
 */
class AbstractService
{

    const PARAM_AUTHORIZATION = "Authorization";

    /**
     * Retorna o endereço do web api 'r2-security'.
     *
     * @return string
     */
    protected function getContextServer($relativePath)
    {
        return getenv('URL_SECURITY')."{$relativePath}";
    }

    /**
     * Retorna o objeto recuperado da 'Request'.
     *
     * @param Request $request
     * @throws SecurityException
     * @return stdClass
     */
    protected function readEntity($request)
    {
        if ($request->code != 200) {
            throw new SecurityException(HttpResponseCode::http_response_code($request->code), $request->code);
        }
        return $request->body;
    }
}
