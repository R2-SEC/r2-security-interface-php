<?php
namespace R2Soft\Security;
/*
 * EmpresaService.php
 * Copyright (c) R2 Soft Informatica e Softwares Ltda.
 *
 * Este software é confidencial e propriedade da R2 Soft Informatica e Softwares Ltda.
 * Não é permitida sua distribuição ou divulgação do seu conteúdo sem expressa autorização da R2 Soft Informatica e Softwares Ltda.
 * Este arquivo contém informações proprietárias.
 */
use Httpful\Request;
use R2Soft\Security\Utils\Token;

/**
 * Classe de serviço refernete a Empresa.
 *
 * @author Paulo Leonardo de O. Miranda
 */
class EmpresaService extends AbstractService
{

    /**
     * Fabrica de instâncias de 'EmpresaService'.
     *
     * @return \App\Service\EmpresaService
     */
    public static function newInstance()
    {
        return new EmpresaService();
    }

    /**
     * Retorna a instância de 'Empresa' conforme o id.
     *
     * @param integer $idEmpresa
     * @param string $token
     */
    public function getEmpresa($idEmpresa, $token)
    {
        $url = $this->getContextServer("empresas/{$idEmpresa}");
        $request = Request::get($url)->addHeader(static::PARAM_AUTHORIZATION, "Bearer $token")->send();
        return $this->readEntity($request);
    }

    public function getConfiguracoesDatabasePorIdUsuarioEmpresa($token)
    {
        $idUsuarioEmpresa = Token::getValueOfPayload('idUsuarioEmpresaReferencia', $token);
        $url = $this->getContextServer("configuracoesDatabases/usuarioEmpresa/{$idUsuarioEmpresa}");
        $request = Request::get($url)->addHeader(static::PARAM_AUTHORIZATION, "Bearer $token")->send();
        return $this->readEntity($request);
    }
}
