<?php
/*
 * SecurityException.php
 * Copyright (c) R2 Soft Informatica e Softwares Ltda.
 *
 * Este software é confidencial e propriedade da R2 Soft Informatica e Softwares Ltda.
 * Não é permitida sua distribuição ou divulgação do seu conteúdo sem expressa autorização da R2 Soft Informatica e Softwares Ltda.
 * Este arquivo contém informações proprietárias.
 */
namespace R2Soft\Security;

use Exception;

/**
 * Classe padrão para exceções de negócio da aplicação.
 *
 * @author Paulo Leonardo de O. Miranda
 */
class SecurityException extends Exception
{

    /**
     * Construtor da classe.
     *
     * @param string $message
     */
    public function __construct($message, $code)
    {
        $this->message = $message;
        $this->code = $code;
    }
}
