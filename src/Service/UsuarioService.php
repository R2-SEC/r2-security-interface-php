<?php
namespace R2Soft\Security;
/*
 * UsuarioService.php
 * Copyright (c) R2 Soft Informatica e Softwares Ltda.
 *
 * Este software é confidencial e propriedade da R2 Soft Informatica e Softwares Ltda.
 * Não é permitida sua distribuição ou divulgação do seu conteúdo sem expressa autorização da R2 Soft Informatica e Softwares Ltda.
 * Este arquivo contém informações proprietárias.
 */
use Httpful\Request;

/**
 * Classe de serviço refernete ao Usuário.
 *
 * @author Paulo Leonardo de O. Miranda
 */
class UsuarioService extends AbstractService
{

    /**
     * Fabrica de instâncias de 'UsuarioService'.
     *
     * @return \App\Service\UsuarioService
     */
    public static function newInstance()
    {
        return new UsuarioService();
    }

    /**
     * Retorna o 'Usuario' conforme o 'id' informado.
     *
     * @param integer $idUsuario
     * @param string $token
     */
    public function getUsuario($idUsuario, $token)
    {
        $url = $this->getContextServer("usuarios/{$idUsuario}");
        $request = Request::get($url)->addHeader(static::PARAM_AUTHORIZATION, "Bearer $token")->send();
        return $this->readEntity($request);
    }

    /**
     * Retorna o informações 'Usuario' extraidas do token.
     *
     * @param string $token
     */
    public function getInfo($token)
    {
        $url = $this->getContextServer("auth/info");
        $request = Request::get($url)->addHeader(static::PARAM_AUTHORIZATION, "Bearer $token")->send();
        return $this->readEntity($request);
    }
}
