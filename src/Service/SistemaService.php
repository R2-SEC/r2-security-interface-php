<?php
/*
 * SistemaService.php
 * Copyright (c) R2 Soft Informatica e Softwares Ltda.
 *
 * Este software é confidencial e propriedade da R2 Soft Informatica e Softwares Ltda.
 * Não é permitida sua distribuição ou divulgação do seu conteúdo sem expressa autorização da R2 Soft Informatica e Softwares Ltda.
 * Este arquivo contém informações proprietárias.
 */
namespace R2Soft\Security;

use Httpful\Request;

/**
 * Classe de serviço refernete ao Sistema.
 *
 * @author Paulo Leonardo de O. Miranda
 */
class SistemaService extends AbstractService
{

    /**
     * Fabrica de instâncias de 'SistemaService'.
     *
     * @return \App\Service\SistemaService
     */
    public static function newInstance()
    {
        return new SistemaService();
    }

    /**
     * Retorna a instância de 'Sistema' conforme o id.
     *
     * @param integer $idSistema
     * @param string $token
     */
    public function getSistema($idSistema, $token)
    {
        $url = $this->getContextServer("sistemas/{$idSistema}");
        $request = Request::get($url)->addHeader(static::PARAM_AUTHORIZATION, "Bearer $token")->send();
        return $this->readEntity($request);
    }
}
