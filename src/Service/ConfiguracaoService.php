<?php
/*
 * ConfiguracaoService.php
 * Copyright (c) R2 Soft Informatica e Softwares Ltda.
 *
 * Este software é confidencial e propriedade da R2 Soft Informatica e Softwares Ltda.
 * Não é permitida sua distribuição ou divulgação do seu conteúdo sem expressa autorização da R2 Soft Informatica e Softwares Ltda.
 * Este arquivo contém informações proprietárias.
 */
namespace R2Soft\Security;

use Httpful\Request;

/**
 * Classe de serviço refernete a Configuração da Empresa.
 *
 * @author Paulo Leonardo de O. Miranda
 */
class ConfiguracaoService extends AbstractService
{

    /**
     * Fabrica de instâncias de 'ConfiguracaoService'.
     *
     * @return \App\Service\ConfiguracaoService
     */
    public static function newInstance()
    {
        return new ConfiguracaoService();
    }

    /**
     * Retorna a instância de 'Configuracao' conforme o id.
     *
     * @param integer $idConfiguracao
     * @param string $token
     */
    public function getConfiguracao($idConfiguracao, $token)
    {
        $url = $this->getContextServer("configuracoes/{$idConfiguracao}");
        $request = Request::get($url)->addHeader(static::PARAM_AUTHORIZATION, "Bearer $token")->send();
        return $this->readEntity($request);
    }

    /**
     * Retorna a 'Configuração' associado ao id do sistema e da empresa
     * informados como parâmetros.
     *
     * @param integer $idEmpresa
     * @param integer $idSistema
     * @param string $token
     */
    public function getConfiguracaoPorEmpresaSistema($idEmpresa, $idSistema, $token)
    {
        $url = $this->getContextServer("configuracoes/empresa/{$idEmpresa}/sistema/{$idSistema}");
        $request = Request::get($url)->addHeader(static::PARAM_AUTHORIZATION, "Bearer $token")->send();
        return $this->readEntity($request);
    }
}
