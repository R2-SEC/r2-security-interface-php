<?php
/*
 * AutenticacaoService.php
 * Copyright (c) R2 Soft Informatica e Softwares Ltda.
 *
 * Este software é confidencial e propriedade da R2 Soft Informatica e Softwares Ltda.
 * Não é permitida sua distribuição ou divulgação do seu conteúdo sem expressa autorização da R2 Soft Informatica e Softwares Ltda.
 * Este arquivo contém informações proprietárias.
 */
namespace R2Soft\Security;

use Exception;
use Httpful\Request;
use R2Soft\Security\Utils\Token;

/**
 * Classe de serviço refernete a Autenticação de Usuário.
 *
 * @author Paulo Leonardo de O. Miranda
 */
class AutenticacaoService extends AbstractService
{

    /**
     * Fabrica de instâncias de 'AutenticacaoService'.
     *
     * @return \App\Service\AutenticacaoService
     */
    public static function newInstance()
    {
        return new AutenticacaoService();
    }

    /**
     * Autentica o 'Usuario' conforme 'token' informado.
     *
     * @param string $token
     */
    public function autenticar($token)
    {
        $url = $this->getContextServer("auth/token/{$token}");
        $request = Request::get($url)->send();
        $usuario = $this->readEntity($request);
//        debug($usuario);
        $usuario->roles = $this->getFuncionalidadesFormatar($usuario);
        return $usuario;
    }

    /**
     * Retorna o array de 'Funcionalidades' formatadas conforme a especificação do projeto.
     *
     * @param \stdClass $usuario
     * @return array
     */
    private function getFuncionalidadesFormatar(\stdClass $usuario)
    {
        $funcionalidades = array();

        foreach ($usuario->funcionalidades as $funcionalidade) {
            $funcionalidades["controllers_{$funcionalidade}"] = array(
                "User_{$usuario->id}" => TRUE
            );
        }
        return $funcionalidades;
    }

    /**
     * Autentica o 'Usuario' conforme 'Client and ClientSecret' informado.
     *
     * @param string $clientId
     * @param string $clientSecret
     * @param integer $idUsuarioEmpresa
     */
    public function autenticarByClient($clientId,$clientSecret,$idUsuarioEmpresa){
        $url = $this->getContextServer("auth/");
        $data = array(
            'grantType' => 'client',
            'clientId' => $clientId,
            'clientSecret' => $clientSecret,
            'idUsuarioEmpresa' => $idUsuarioEmpresa
        );

        $response = Request::post($url)->sendsJson()->body($data)->send();
        if($response->code != '200')
            throw new Exception('Acesso não permitido!');
        return $response->body;
    }

    /**
     * Retorna a chave JWT conforme os parâmetros informados.
     *
     * @param idUsuario
     * @param idEmpresa
     * @param idSistema
     * @return
     */
    public function getTokenAcesso($token)
    {
        $idUsuarioEmpresa = Token::getValueOfPayload('idUsuarioEmpresaReferencia', $token);
        if(!$idUsuarioEmpresa)
            $idUsuarioEmpresa = Token::getValueOfPayload('idUsuarioEmpresa', $token);
        return AutenticacaoService::newInstance()->autenticarByClient(
            getenv('SECURITY_CLIENT_ID'),
            getenv('SECURITY_CLIENT_SECRET'),
            $idUsuarioEmpresa
        );
    }

    public function refreshToken($refreshToken)
    {
        $url = $this->getContextServer("auth/token/{$refreshToken}/refresh");
        $request = Request::get($url)->addHeader(static::PARAM_AUTHORIZATION, "Bearer $refreshToken")->send();

        return $this->readEntity($request);
    }

    public function getUserToken($token){
        $url = $this->getContextServer("auth/token/{$token}");
        $request = Request::get($url)->send();
        return $this->readEntity($request);
    }
}
