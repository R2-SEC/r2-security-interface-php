<?php
$token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiI4ZTU4OTdhMDg1MDcyMjQwMDUwMDZhNjZkYWQyNjgwYyIsImlkVXN1YXJpb0VtcHJlc2EiOjM4MywiYXVkIjoiN2UyMWQ2YWE0NmJlZTRmMTdkZWU2NDhiYzk2ZDc4MjUiLCJ0eXBlIjoiQUNDRVNTIiwiZXhwIjoxNjUxNzg0NTU5LCJpYXQiOjE2NTE3ODQ0Mzl9.oyse3n9HK9zujwWi8c9nUqJ8KxWnM-Hg6gAEBXhRcng";
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require './vendor/autoload.php';

use R2Soft\Security\Utils\AutenticacaoService;
use R2Soft\Security\Utils\ConfiguracaoService;
use R2Soft\Security\Utils\EmpresaService;
use R2Soft\Security\Utils\SistemaService;
use R2Soft\Security\Utils\UsuarioService;
use Symfony\Component\Dotenv\Dotenv;

$dotenv = new Dotenv();
$dotenv->load(__DIR__ . '/.env.exemple');

?>

<html>

<head>
    <title>R2 Security Interface</title>
</head>

<body>

<h1 style="font-family: monospace;">R2 Security Interface</h1>

<ul style="font-family: monospace;">
    <li><b>AutenticacaoService</b></li>

    <li style="list-style: none;">
        <ul style="padding-top: 10px;">
            <li style="list-style: none;"><i>AutenticacaoService::newInstance()->autenticar($token);</i></li>
            <li style="list-style: none;border: #000000 solid 1px; padding: 10px; margin: 20px 0px;">
                <?php
                try {
                    $usuario = AutenticacaoService::newInstance()->autenticarByClient(
                        '02ba8390-e300-40ae-8aaf-71cd14bbc5f5',
                        '433533291ecadb2c2d0c7ccd2fad825f9a25a9fab65165aa2f6c00a6b41e7662',
                        2320
                    );
                    debug($usuario, false);
                } catch (Exception $e) {
                    debug($e->getMessage(), false);
                }

                // AutenticacaoService
                try {
                    $usuario = AutenticacaoService::newInstance()->autenticar($token);
                    debug($usuario, false);
                } catch (Exception $e) {
                    debug($e->getMessage(), false);
                }


                ?>
            </li>

            <li style="list-style: none;"><i>AutenticacaoService::newInstance()->getInfo($token);</i></li>
            <li style="list-style: none;border: #000000 solid 1px; padding: 10px; margin: 20px 0px;">
                <?php
                // AutenticacaoService
                try {
                    $info = UsuarioService::newInstance()->getUsuario($usuario->id,$usuario->accessToken);
                    debug($info, false);
                    $info = UsuarioService::newInstance()->getInfo($usuario->accessToken);
                    debug($info, false);
                } catch (Exception $e) {
                    debug($e->getMessage(), false);
                }


                ?>
            </li>
        </ul>
    </li>

    <li><b>ConfiguracaoService</b></li>

    <li style="list-style: none;">
        <ul style="padding-top: 10px;">
            <li style="list-style: none;"><i>ConfiguracaoService::newInstance()->getConfiguracaoPorEmpresaSistema($idEmpresa,
                    $idSistema, $token);</i></li>

            <li style="list-style: none;border: #000000 solid 1px; padding: 10px; margin: 20px 0px;">
                <?php
                try {
                    $configuracao = ConfiguracaoService::newInstance()->getConfiguracaoPorEmpresaSistema($usuario->empresa->id, $usuario->sistema->id, $usuario->accessToken);
                    debug($configuracao, false);
                } catch (Exception $e) {
                    debug($e->getMessage(), false);
                }

                ?>
            </li>
            <li style="list-style: none;"><i>ConfiguracaoService::newInstance()->getConfiguracao($idConfiguracao,
                    $token);</i></li>

            <li style="list-style: none;border: #000000 solid 1px; padding: 10px; margin: 20px 0px;">
                <?php
                try {
                    // ConfiguracaoService
                    $novaConfiguracao = ConfiguracaoService::newInstance()->getConfiguracao($configuracao->id, $usuario->accessToken);
                    debug($novaConfiguracao, false);
                }catch (Exception $e){
                    debug($e->getMessage(), false);
                }

                ?>
            </li>
        </ul>
    </li>

    <li><b>EmpresaService</b></li>

    <li style="list-style: none;">
        <ul style="padding-top: 10px;">
            <li style="list-style: none;"><i>EmpresaService::newInstance()->getEmpresa($idEmpresa, $token);</i></li>
            <li style="list-style: none;border: #000000 solid 1px; padding: 10px; margin: 20px 0px;">
                <?php
                try {
                    // EmpresaService
                    $empresa = EmpresaService::newInstance()->getEmpresa($usuario->empresa->id, $usuario->accessToken);
                    debug($empresa, false);
                }catch (Exception $e){
                    debug($e->getMessage(), false);
                }

                ?>
            </li>
        </ul>
    </li>

    <li><b>SistemaService</b></li>

    <li style="list-style: none;">
        <ul style="padding-top: 10px;">
            <li style="list-style: none;"><i>SistemaService::newInstance()->getSistema($idSistema, $token);</i></li>
            <li style="list-style: none;border: #000000 solid 1px; padding: 10px; margin: 20px 0px;">
                <?php
                try {
                    // SistemaService
                    $sistema = SistemaService::newInstance()->getSistema($usuario->sistema->id, $usuario->accessToken);
                    debug($sistema, false);
                }catch (Exception $e){
                    debug($e->getMessage(), false);
                }

                ?>
            </li>
        </ul>
    </li>

    <li><b>UsuarioService</b></li>

    <li style="list-style: none;">
        <ul style="padding-top: 10px;">
            <li style="list-style: none;"><i>UsuarioService::newInstance()->getUsuario($idUsuario, $token);</i></li>
            <li style="list-style: none;border: #000000 solid 1px; padding: 10px; margin: 20px 0px;">
                <?php
                try {
                    // UsuarioService
                    $novoUsuario = UsuarioService::newInstance()->getUsuario($usuario->id, $usuario->accessToken);
                    debug($novoUsuario, false);
                }catch (Exception $e){
                    debug($e->getMessage(), false);
                }

                ?>
            </li>
        </ul>

    </li>
</ul>

</body>

</html>
